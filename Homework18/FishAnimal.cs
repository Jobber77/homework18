﻿using System;

namespace Homework18
{
    public class FishAnimal : Animal
    {
        public int FinAmount { get; set; }

        public FishAnimal() {}

        public FishAnimal(int finAmount) : base(AnimalType.Swiming)
        {
            FinAmount = finAmount;
        }

        public override object Clone() => (object)CloneTyped<FishAnimal>();

        public override T CloneTyped<T>() =>
            this is T ? new FishAnimal(FinAmount) as T : throw new ArgumentException("Can not convert object to specified type");

        public override string ToString() => $"FishAnimal {{ FinAmount: {FinAmount}, Type: {Type} }}";

    }
}
