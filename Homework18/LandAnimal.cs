﻿using System;

namespace Homework18
{
    public class LandAnimal : Animal
    {
        public int LegsAmount { get; set; }

        public LandAnimal() { }

        public LandAnimal(int legsAmount) : base(AnimalType.Walking)
        {
            LegsAmount = legsAmount;
        }

        public override object Clone() => (object)CloneTyped<LandAnimal>();

        public override T CloneTyped<T>() 
            => this is T ? new LandAnimal(LegsAmount) as T : throw new ArgumentException("Can not convert object to specified type");
    }
}
