﻿using System;

namespace Homework18
{
    public class Animal : IMyCloneable
    {
        public AnimalType Type { get; set; }

        public Animal() { }

        public Animal(AnimalType type)
        {
            Type = type;
        }

        public virtual object Clone() => (object)CloneTyped<Animal>();

        public virtual T CloneTyped<T>() where T : class, IMyCloneable, new()
        {
            return this is T ? new Animal(Type) as T : throw new ArgumentException("Can not convert object to specified type");
        }
    }
}
