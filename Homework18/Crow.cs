﻿using System;

namespace Homework18
{
    public class Crow : BirdAnimal
    {
        public string Color { get; set; }

        public Crow() { }

        public Crow(int weight, string color) : base(weight)
        {
            Color = color;
        }

        public override object Clone() => (object)CloneTyped<Crow>();

        public override T CloneTyped<T>()
            => this is T ? new Crow(Weight, Color) as T : throw new ArgumentException("Can not convert object to specified type");

        public override string ToString() => $"Crow {{ Color: {Color}, Weight: {Weight}, Type: {Type} }}";

    }
}
