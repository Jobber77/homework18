﻿using System;

namespace Homework18
{
    class Program
    {
        static void Main(string[] args)
        {
            FishAnimal dangerShark = new Shark(5, true);
            var hugeMonkey = new Monkey(true);
            var crow = new Crow(10500, "black");
            Console.WriteLine("objects before copy:");
            Console.WriteLine(dangerShark);
            Console.WriteLine(hugeMonkey);
            Console.WriteLine(crow);
            Console.WriteLine("make objects copies..");
            var copyShark = dangerShark.CloneTyped<Shark>();
            var copyMonkey = hugeMonkey.CloneTyped<Monkey>();
            var copyCrow = crow.CloneTyped<Crow>();
            Console.WriteLine("copied objects:");
            Console.WriteLine(copyShark);
            Console.WriteLine(copyMonkey);
            Console.WriteLine(copyCrow);
            Console.WriteLine("modify copied objects..");
            copyShark.FinAmount = 1234456;
            copyMonkey.IsBigOne = false;
            copyCrow.Weight = 1;
            Console.WriteLine("copy objects objects after copies modification:");
            Console.WriteLine(copyShark);
            Console.WriteLine(copyMonkey);
            Console.WriteLine(copyCrow);
            Console.WriteLine("source objects after copies modification:");
            Console.WriteLine(dangerShark);
            Console.WriteLine(hugeMonkey);
            Console.WriteLine(crow);

            Console.ReadKey();
        }
    }
}
