﻿using System;

namespace Homework18
{
    public interface IMyCloneable : ICloneable
    {
        T CloneTyped<T>() where T : class, IMyCloneable, new();
    }
}
