﻿using System;

namespace Homework18
{
    public class Monkey : LandAnimal
    {
        public bool IsBigOne { get; set; }

        public Monkey() { }

        public Monkey(bool isBig) : base(2)
        {
            IsBigOne = isBig;
        }

        public override object Clone() => (object)CloneTyped<Monkey>();

        public override T CloneTyped<T>()
            => this is T ? new Monkey(IsBigOne) as T : throw new ArgumentException("Can not convert object to specified type");

        public override string ToString() => $"Monkey {{ IsBigOne: {IsBigOne}, LegsAmount: {LegsAmount}, Type: {Type} }}";

    }
}
