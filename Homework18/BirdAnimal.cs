﻿using System;

namespace Homework18
{
    public class BirdAnimal : Animal
    {
        public int Weight { get; set; }

        public BirdAnimal() { }

        public BirdAnimal(int weight) : base(AnimalType.Flying)
        {
            Weight = weight;
        }
        public override object Clone() => (object)CloneTyped<BirdAnimal>();

        public override T CloneTyped<T>() =>
            this is T ? new BirdAnimal(Weight) as T : throw new ArgumentException("Can not convert object to specified type");
    }
}
