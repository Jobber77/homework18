﻿using System;

namespace Homework18
{
    public class Shark : FishAnimal
    {
        public bool IsDangerForHuman { get; set; }

        public Shark() { }

        public Shark(int finAmount, bool isDangerForHuman) : base(finAmount)
        {
            IsDangerForHuman = isDangerForHuman;
        }

        public override object Clone() => (object)CloneTyped<Shark>();

        public override T CloneTyped<T>()
            => this is T ? new Shark(FinAmount, IsDangerForHuman) as T : throw new ArgumentException("Can not convert object to specified type");

        public override string ToString() => $"Shark {{ IsDangerForHuman: {IsDangerForHuman}, FinAmount: {FinAmount}, Type: {Type} }}";
    }
}
